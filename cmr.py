import requests
from requests.auth import HTTPBasicAuth
import sys
import logging

# Constants
BASE_URL = "https://lendingkart.atlassian.net"
URL_CHECK_STATUS = f"{BASE_URL}/rest/api/2/issue/{{ticket_id}}"
URL_POST_COMMENT = f"{BASE_URL}/rest/api/2/issue/{{ticket_id}}/comment"

# Configure logging
logging.basicConfig(level=logging.INFO, format="[%(levelname)s] %(message)s")
logger = logging.getLogger(__name__)

def get_ticket_status(ticket_id, username, password, desired_status):
    # Jira REST API endpoint for getting issue details
    url = URL_CHECK_STATUS.format(ticket_id=ticket_id)
    # Set the request headers and body
    headers = {"Content-Type": "application/json"}
    try:
        # Send a GET request to the Jira issue endpoint with basic authentication
        response = requests.get(url, headers=headers, auth=HTTPBasicAuth(username, password))
        logger.info("get_ticket_status_response: %s", response)

        # Check if the request was successful (status code 200)
        if response.status_code == 200:
            issue = response.json()
            status = issue["fields"]["status"]["name"].lower()
            logger.info("Actual status: %s, Expected status: %s", status, desired_status.lower())
            return desired_status.lower() in status

        logger.info("Failed to retrieve issue details. Status code: %s", response.status_code)
        raise RuntimeError(f"Failed to retrieve ticket '{ticket_id}' details. Status code: {response.status_code}")
    except requests.exceptions.RequestException as e:
        logger.error("An error occurred: %s", e)
        raise

def add_comment_to_jira(url, comment_body, username, password):
    api_url = url
    headers = {"Content-Type": "application/json"}
    data = {"body": comment_body}

    try:
        response = requests.post(api_url, headers=headers, auth=HTTPBasicAuth(username, password), json=data)
        if response.status_code == 201:
            logger.info("Jira comment added successfully.")
        else:
            logger.info("Failed to add Jira comment. Status code: %s, Error message: %s", response.status_code, response.text)
    except requests.exceptions.RequestException as e:
        logger.error("Error occurred: %s", e)

def main(project, user_name, pwd, ticket_id, desired_status, pipeline_url, pipeline_triggered_by):
    user_name = user_name
    pwd = pwd
    base_url = "https://lendingkart.atlassian.net"
    url_check_status = URL_CHECK_STATUS.format(ticket_id=ticket_id)
    url_post_comment = URL_POST_COMMENT.format(ticket_id=ticket_id)

    if 'approved' in desired_status.lower():
        comment_to_post = f"Gitlab comment - The pipeline '{pipeline_url}' was triggered by '{pipeline_triggered_by}' for 'production deployment'"
    elif 'rollback' in desired_status.lower():
        comment_to_post = f"Gitlab comment - The pipeline '{pipeline_url}' was triggered by '{pipeline_triggered_by}' for 'production rollback'"
    else:
        comment_to_post = f"Gitlab comment - The pipeline '{pipeline_url}' was triggered by '{pipeline_triggered_by}' for '{desired_status}'"

    ticket_current_status = get_ticket_status(ticket_id, user_name, pwd, desired_status)
    add_comment_to_jira(url_post_comment, comment_to_post, user_name, pwd)

    if ticket_current_status:
        logger.info("The current status of the ticket %s is '%s'.", ticket_id, desired_status)
        return True
    else:
        logger.info("The current status of the ticket %s is NOT '%s'.", ticket_id, desired_status)
        return False

if __name__ == '__main__':
    project = "CMR"

    # Check if there are enough command line arguments
    if len(sys.argv) < 7:
        raise RuntimeError("[ERROR] Invalid number of arguments. Please provide at least CMR Ticket ID, Desired Status, CI Pipeline URL, GitLab User Email, Jira User Name, and Jira API Token.")

    # Initialize variables
    ticket_id, desired_status, CI_PIPELINE_URL, GITLAB_USER_EMAIL, JIRA_USER_NAME = None, None, None, None, None

    # Loop through command line arguments and perform checks
    for arg in sys.argv[1:]:
        # Check for CMR Ticket ID
        if arg.startswith("CMR-"):
            ticket_id = arg
        # Check for Desired Status
        elif arg.lower() in ["approved", "rollback required", "rollbackrequired"]:
            desired_status = arg.lower()
        # Check for CI Pipeline URL
        elif arg.startswith("https://") and "gitlab" in arg:
            CI_PIPELINE_URL = arg
        # Check for GITLAB_USER_EMAIL
        elif not arg.startswith("automation.user") and arg.endswith("lendingkart.com"):
            GITLAB_USER_EMAIL = arg
        # Check for JIRA_USER_NAME
        elif arg.startswith("automation.user") and arg.endswith("lendingkart.com"):
            JIRA_USER_NAME = arg

    JIRA_API_TOKEN = sys.argv[-1]

    # Validate the values
    if None in [ticket_id, desired_status, CI_PIPELINE_URL, GITLAB_USER_EMAIL, JIRA_USER_NAME, JIRA_API_TOKEN]:
        raise RuntimeError("[ERROR] Invalid arguments. Please provide valid values for CMR Ticket ID, Desired Status, CI Pipeline URL, GitLab User Email, Jira User Name, and Jira API Token.")

    # Construct Jira API URL
    url = URL_CHECK_STATUS.format(ticket_id=ticket_id)

    # Call the main function
    final_ticket_status = main(project, JIRA_USER_NAME, JIRA_API_TOKEN, ticket_id, desired_status, CI_PIPELINE_URL, GITLAB_USER_EMAIL)

    logger.info("final_ticket_status: %s", final_ticket_status)

    if not final_ticket_status:
        logger.error(f"The current status of the ticket {ticket_id} is NOT '{desired_status}'. CMR check is Failed. Aborting prod-deployment action!")
        sys.exit(1)
    else:
        logger.info(f"The CMR ticket {ticket_id} is Approved. CMR check is Passed")

        
