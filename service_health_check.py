import sys
import requests
import logging
from urllib.parse import urljoin

# Constants
TIMEOUT = 5
BASE_URLS = [
    "https://preprod.lendingkart.com/",
    "https://preprod-cluster.lendingkart.com/",
    "https://preprod-cluster-internal.lendingkart.com/",
    "https://preprod-internal.lendingkart.com/",
    "https://preprod-elb.lendingkart.com/"
]

# Configure Logging with custom log format
logging.basicConfig(level=logging.DEBUG, format='%(levelname)s:%(name)s:%(message)s')
logger = logging.getLogger(__name__)
urllib3_logger = logging.getLogger("urllib3")
urllib3_logger.setLevel(logging.WARNING)


def construct_endpoint(service_name):
    if service_name == "forge-pdf" or service_name == "ape":
        return f"{service_name}/health-check"
    elif service_name in ["partner-consumer", "ops-consumer", "cipher-cdc-consumer", "customer-consumer",
                          "whatsapp-cipher-consumer"]:
        return f"{service_name}/health"
    elif service_name in ["whatsapp-cdc-consumer", "third-party-consumer"]:
        return f"{service_name}/"
    else:
        return f"{service_name}/actuator/health"


def construct_url(service_name, base_url):
    if service_name == 'DS':
        return "http://172.18.18.82:8001/scoring/health-check"
    else:
        return urljoin(base_url, construct_endpoint(service_name))


def check_health(service_name, base_url):
    url = construct_url(service_name, base_url)

    try:
        response = requests.get(url, timeout=TIMEOUT)
        logger.debug(f"Request URL: {url}, Status Code: {response.status_code}")

        if response.ok:
            return "UP"
        elif response.status_code >= 500:
            return "DOWN"
        return f"DOWN ({response.status_code})"
    except requests.exceptions.Timeout as e:
        logger.error(f"Connection timed out while requesting URL: {url}")
        logger.error(str(e))
        return "DOWN (Connection Timeout)"
    except requests.exceptions.ConnectionError as e:
        logger.error(f"Connection error occurred while requesting URL: {url}")
        logger.error(str(e))
        return "DOWN (Connection Error)"
    except requests.exceptions.RequestException as e:
        logger.error(f"Error occurred while requesting URL: {url}")
        logger.error(str(e))
        return "DOWN"


def main(service_name):
    service_found = False

    for base_url in BASE_URLS:
        health_status = check_health(service_name, base_url)
        if "DOWN" not in health_status:
            print(f"Service: {service_name}, Status: {health_status}")
            service_found = True

        if health_status == "DOWN":
            return False
        elif health_status == "UP":
            return True

    if not service_found:
        print(f"Service: {service_name}, Status: DOWN")
        logger.error(f"Service: {service_name}, Status: DOWN")
        return False


if __name__ == "__main__":
    if len(sys.argv) != 2:
        logger.error("Please provide a service-name as a command-line argument")
        sys.exit(1)
    else:
        service_name = sys.argv[1]
        service_health_status = main(service_name)

        if not service_health_status:
            logger.warning(f"Current status of the service '{service_name}' in Pre-prod is not 'UP'.")
        else:
            logger.info(f"Pre-prod service health check for '{service_name}' completed!")
