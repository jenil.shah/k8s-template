#!/bin/bash
KUBECTL_CMD="kubectl apply -f"
CONFIGMAP_API_VERSION="v1"
SECRET_API_VERSION="bitnami.com/v1alpha1"
while IFS= read -r line; do
   NAME=`echo $line | awk -F / '{print$2}'`

   if [ $NAME == "app" ] || [ $NAME == "cis" ] || [ $NAME == "webhooks" ]
   then
       aws eks --region ap-south-1 update-kubeconfig --name  prod-k8s-eks-cluster
       sed -i "s/__CONFIGMAP_API_VERSION__/$CONFIGMAP_API_VERSION/g" $line
#       sed -i "s/__SECRET_API_VERSION__/$SECRET_API_VERSION/g" $line
       sed -i "s/__CI_ENVIRONMENT_SLUG__/$NAME/g" $line
       echo "The file Deploying in Production is : "  $line
       $KUBECTL_CMD $line
   elif [ $NAME == "preprod" ]
   then
       aws eks --region ap-south-1 update-kubeconfig --name preprod-cluster
       sed -i "s/__CONFIGMAP_API_VERSION__/$CONFIGMAP_API_VERSION/g" $line
#       sed -i "s/__SECRET_API_VERSION__/$SECRET_API_VERSION/g" $line
       sed -i "s/__CI_ENVIRONMENT_SLUG__/$NAME/g" $line
       echo "The file Deploying in Preproduction is : "  $line
       $KUBECTL_CMD $line
   else
       aws eks --region ap-south-1 update-kubeconfig --name qa-k8s-eks-cluster
       sed -i "s/__CONFIGMAP_API_VERSION__/$CONFIGMAP_API_VERSION/g" $line
#       sed -i "s/__SECRET_API_VERSION__/$SECRET_API_VERSION/g" $line
       sed -i "s/__CI_ENVIRONMENT_SLUG__/$NAME/g" $line
       echo "The file Deploying in QA is: "  $line
       $KUBECTL_CMD $line
   fi
done < "modified_configs.txt"
